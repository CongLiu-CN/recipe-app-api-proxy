# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

It is commonly used best pratice to add configurations here in order to let somebody who is new to this project understand how to get this app running.

### Environment Variables

 - `LISTEN_PORT` - Port to listen on (default: `8000`)
 - `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
 - `APP_PORT` - Port of the app to forward requests to (default: `9000`)

 ## File Structure

 ### .gitlab-ci.yml

 This will set up CI/CD on GitLab.